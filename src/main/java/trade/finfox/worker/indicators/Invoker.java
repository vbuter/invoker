package trade.finfox.worker.indicators;

import java.util.Objects;

public class Invoker {

    private InvokerHandler handler;

    /**
     * При установке InvokerHandler происходит вызов onTrigger в режиме
     * инициализации. В этом режиме срабатывают все условные переходы, все
     * инструкции у switch_ и т.п. Это необходимо чтобы в рабочем режиме
     * исключить дополнительные задержи и чтобы при написании тригера его
     * инициализацию можно было делать по ходу описания логики.
     *
     * @param handler
     */
    public Invoker(InvokerHandler handler) {
	Objects.requireNonNull(handler);
//	handler.init();
	this.handler = handler;
    }
    
    public void setHandler(InvokerHandler handler) {
	Objects.requireNonNull(handler);
	this.handler = handler;
    }

    public void invoke(Indicator indicator) {
	handler.putIndicator(indicator);
	handler.onTrigger();
    }

}
