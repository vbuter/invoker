package trade.finfox.worker.indicators;

/**
 * Простой Java объект
 *
 * @author vab
 */
public class Indicator extends Value {

    private String name;
    private Long time;

    /**
     *
     * @param name Название
     * @param time Метка времени в наносекундах
     */
    public Indicator(String name, Long time) {
	this.name = name;
	this.time = time;
    }
    /**
     *
     * @param name Название
     * @param value Значение
     * @param time Метка времени в наносекундах
     */
    public Indicator(String name, Double value, Long time) {
	this.name = name;
	if (value != null) {
	    this.val = value;
	}
	this.time = time;
    }

    public String getName() {
	return name;
    }

    public Indicator setName(String name) {
	this.name = name;
	return this;
    }

    public Double getValue() {
	return this.val;
    }

    public Indicator setValue(Double value) {
	this.val = value;
	return this;
    }

    public Long getTime() {
	return time;
    }

    public Indicator setTime(Long time) {
	this.time = time;
	return this;
    }

}
