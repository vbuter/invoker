package trade.finfox.worker.indicators;

import java.util.Collections;
import java.util.List;

public class Table extends Value {

    
    /**
     * Инициализация табличной функции, по входящим параметрам определяем
     * однмоерная таблица или двухмерная. вычисление по таблице происходит по
     * верхней грани.
     *
     * @param v
     * @return
     */
    public Table init(V... v){	
	return new Table();
    }
    public List init3(V... v){	
	return Collections.EMPTY_LIST;
    }

    /**
     * Аналогично init но вычисление по нижней грани.
     *
     * @param v
     * @return
     */
    public Table initLower(V... v){
	return new Table();
    }
}
