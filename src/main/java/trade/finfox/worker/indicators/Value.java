package trade.finfox.worker.indicators;

import java.util.Objects;

public class Value {

    protected Double val = Double.NaN;

    public Value() {
    }

    public Value(Double value) {
	this.val = value;
    }

    public boolean isUndefined() {
	return this.val == null || Objects.equals(this.val, Double.NaN);
    }

    /**
     * Возвращает объект который соотв арифметической разнице между родительским
     * значением Value и дочерним значением Value, которое именновано как name,
     * значение дочернего Value определеятся в initiator
     *
     * @param name
     * @param initiator
     * @return
     */
    public Value diff(String name, Value initiator) {
	try {
	    Double val_ = Double.sum(this.val, -initiator.val);
	    return new Value(val_);
	} catch (Exception e) {
	    return new Value();
	}
    }

    /**
     * Устанавливает текущее значение и возвращает себя
     *
     * @param value
     * @return
     */
    public Value value(double value) {
	this.val = value;
	return this;
    }

    /**
     * Возвращает двухмерную табличную функцию зафисящую от значения родителя и
     * значения param
     *
     * @param initiator
     * @param param
     * @return
     */
    public Table table(Value initiator, Value param) {
	Table table = new Table();
	return table;
    }

    /**
     * Вызывает калбек соотв. начению родителя, возвращает себя
     *
     * @param values
     * @return
     */
    public Value select(Value... values){
	for(Value v : values){
	    // ????
	}
	return this;
    }

    /**
     * Проверяет значение родителя на >= value и вызывает success, возвращает
     * себя
     *
     * @param value
     * @param success
     * @return
     */
    public Value moreEqual(int value, Runnable success){
	if(success != null){
	    success.run();
	}
	return this;	
    }
}
