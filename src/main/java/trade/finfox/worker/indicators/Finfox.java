package trade.finfox.worker.indicators;

public class Finfox {

    public static void main(String[] args) {
	// Приложение создает инстансы Invoker и в дальнейшем вызывает invoke передавая имя индекатора и его значение. 
	// При каждом вызове invoke вызов должен передаваться на handler.onTrigger. 
	// Два разнык потока не могут вызвать invoke в один момент времени, за это отвечает приложение.
	Object[] dataForTable = new Object[]{3000, new Object[]{"TEST", 0}, new Object[]{"AGGRESS", 1000}};
	InvokerHandler handler = new InvokerHandler() {
	    @Override
	    public void onTrigger() {
		// Тут рабочая логика обработчика		 
		MyTable t = new MyTable();
		t.setData(dataForTable);
	    }	   
	};
	Invoker invoker = new Invoker(handler);
	invoker.invoke(new Indicator("indi1", 12D, System.nanoTime()));
	invoker.invoke(new Indicator("indi2", 45D, System.nanoTime()));
	invoker.invoke(new Indicator("indi3", 90D, System.nanoTime()));
	
    }
}
