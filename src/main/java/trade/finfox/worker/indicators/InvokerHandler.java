package trade.finfox.worker.indicators;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

public abstract class InvokerHandler {

    private final Map<String, Indicator> indicators = new HashMap<>();

    /**
     * Добавляет индикатор с заданным значением. Если значение уже установлено,
     * то оно обновляется.
     *
     * @param indicator
     * @return Переданный индикатор
     */
    protected Indicator putIndicator(Indicator indicator) {
	indicators.put(indicator.getName(), indicator);
	return indicator;
    }

    /**
     * Очищает список накопленных индикаторов.
     */
    protected void clearIndicators() {
	indicators.clear();
    }

    /**
     * Возвращает индекатор по указанному имени, если значение индекатора
     * определено то вызывается калбек
     *
     * @param name
     * @param callback
     * @return null если нет индикатора с таким именем
     */
    protected Indicator ifi(String name, Consumer<Indicator> callback) {
	Indicator i = indicators.get(name);
	if (i != null) {
	    if (!i.isUndefined() && callback != null) {
		callback.accept(i);
	    }
	    return i;
	} else {
	    return putIndicator(new Indicator(name, System.nanoTime()));
	}
    }

    /**
     * каждый индекатор имеет метку времени, возвращается значение задержки
     * между самым ранним и самым поздним
     *
     * @param i
     * @return Период задержки в наносекундах
     */
    protected Value timeout(Indicator... i) {
	Long min = null;
	Long max = null;
	for (Indicator ind : i) {
	    Long t = ind.getTime();
	    if (t != null) {
		if (min == null) {
		    min = t;
		} else {
		    min = Long.min(t, min);
		}
		if (max == null) {
		    max = t;
		} else {
		    max = Long.max(t, max);
		}
	    }
	}
	if (min != null && max != null) {
	    Long v = Long.sum(max, -min);
	    return new Value(v.doubleValue());
	} else {
	    return new Value();
	}
    }

    /**
     * Возвращает команду по указанному имени
     *
     * @param name
     * @param initiator
     * @return
     */
    protected Command command(String name, Consumer<Indicator> initiator) {
	Indicator i = indicators.get(name);
	if (i != null) {
	    if (!i.isUndefined() && initiator != null) {
		initiator.accept(i);
	    }
	    return new Command();
	} else {
	    return null;
	}
    }

    public abstract void onTrigger();

}
