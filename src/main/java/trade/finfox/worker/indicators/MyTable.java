package trade.finfox.worker.indicators;

import java.util.NavigableMap;
import java.util.Objects;
import java.util.TreeMap;

public class MyTable {

    private NavigableMap<Object, NavigableMap<Object, Object>> data = new TreeMap<>();

    /**
     * Принимает массив данных в качестве таблицы, преобразовывает его в Map для
     * последующего быстрого извлечения значения
     *
     * @param data
     * @return
     */
    public MyTable setData(Object[] data) {
	this.data = new TreeMap<>();
	for (Object obj : data) {
	    // идём по таблице по строкам
	    Object[] arr = (Object[]) obj;
	    Object value1 = arr[0];
	    for (int i = 1; i < arr.length; i++) {
		Object[] map2 = (Object[]) arr[i];
		this.data.compute(value1, (val1, map) -> {
		    if (map == null) {
			map = new TreeMap<>();
		    }
		    Object value = map2[0];
		    Object value2 = map2[1];
		    map.put(value2, value);
		    return map;
		});
	    }
	}
	return this;
    }

    /**
     * Метод возвращает точное или меньшее найденное значение (например, TEST
     * или NORMAL) для значений value1 и value2
     *
     * @param value1
     * @param value2
     * @return
     */
    public Object get(Object value1, Object value2) {
	Objects.requireNonNull(value1);
	Objects.requireNonNull(value2);
	try {
	    return data.floorEntry(value1).getValue().floorEntry(value2).getValue();
	} catch (Exception e) {
	    return null;
	    // ну или UNDEFINED или свой тип Exception выбросить
	}
    }

}
