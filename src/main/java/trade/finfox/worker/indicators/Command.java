package trade.finfox.worker.indicators;

public class Command {

    /**
     * Добавляет группу с IP адресами для последующей рассылки на них сообщений
     *
     * @param name
     * @return
     */
    public Command addSenderGroup(String name){
	return this;
    }

    /**
     * Задает параметры команде
     *
     * @param dir
     * @param power
     * @return
     */
    public Command set(int dir, int power){
	return this;
    }

    /**
     * Отсылает пакет данных, только в том случае если ранее пакет не отсылался
     *
     * @return
     */
    public Command sendOnce(){
	return this;
    }
}
